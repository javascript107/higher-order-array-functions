function filter(elements, cb) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
    filteredArray=[]
    for (let index=0;index<elements.length; index++){
        result=cb(elements,index)
        if(result===true){
           filteredArray.push(elements[index])
        }
    }
    if(filteredArray.length===0){
        return []
    }else{
        return filteredArray
    }

}

module.exports = filter