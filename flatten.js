function flatten(elements) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
        let flattenArr = [];
        for(let index=0;index<elements.length;index++){
            if(Array.isArray(elements[index])){
                const result = flatten(elements[index]);
                for(let index=0;index<result.length;index++){
                    flattenArr.push(result[index])
                }
            }else{
                flattenArr.push(elements[index])
            }
         };
        return flattenArr;
     
    
}

module.exports=flatten